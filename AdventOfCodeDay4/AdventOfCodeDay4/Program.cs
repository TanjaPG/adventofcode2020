﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCodeDay4
{
    class Program
    {
        static void Main(string[] args)
        {
            var file = input();
            var passportByLine = CovertToArrayOfStrings(file);
            var passport = ConvertToClass(passportByLine);
            var countedOkPassport = CheckPassport(passport);
            var countedOkPassportValue = CheckPassportValue(countedOkPassport);
            Console.WriteLine(countedOkPassport.Count.ToString());
            Console.WriteLine(countedOkPassportValue.ToString());
        }

        public static string input()
        {
            var passwordFile = File.ReadAllText(@"Day4.txt");
            return passwordFile;
        }

        public static string[] CovertToArrayOfStrings(string input)
        {
            string[] separatingStrings = { "\r\n\r\n" };

            var list = input.Split(separatingStrings, System.StringSplitOptions.RemoveEmptyEntries);

            return list;

        }

        public static List<Passport> ConvertToClass(string[] input)
        {

            string[] separatingStrings = { "\r\n", " " };
            List<Passport> ListOfPassports = new List<Passport>();

            foreach (string line in input)
            {
                var list = line.Split(separatingStrings, System.StringSplitOptions.RemoveEmptyEntries);

                Passport passport = new Passport();

                foreach (string split in list)
                {
                    var keyValue = split.Split(":");

                    switch(keyValue[0])
                    {
                        case "byr":
                            passport.BirthYear = keyValue[1];
                            break;
                        case "iyr":
                            passport.IssueYear = keyValue[1];
                            break;
                        case "eyr":
                            passport.ExpirationsYear = keyValue[1];
                            break;
                        case "hgt":
                            passport.Height = keyValue[1];
                            break;
                        case "hcl":
                            passport.HairColor = keyValue[1];
                            break;
                        case "ecl":
                            passport.EyeColor = keyValue[1];
                            break;
                        case "pid":
                            passport.PassportID = keyValue[1];
                            break;
                        case "cid":
                            passport.CountryID = Int32.Parse(keyValue[1]);
                            break;
                    }
                }
                ListOfPassports.Add(passport);
            }
            return ListOfPassports;
        }

        public static List<Passport> CheckPassport(List<Passport> PassportList)
        {
            List<Passport> OkPassportList = new List<Passport>();

            foreach (Passport Passport in PassportList)
            {
               if(Passport.isPassportOk())
                {
                    OkPassportList.Add(Passport);
                }
            }
            return OkPassportList;
        }

        public static int CheckPassportValue(List<Passport> PassportList)
        {
            var counter = 0;

            foreach (Passport Passport in PassportList)
            {
                if (Passport.isValueInPassportOk())
                {
                    counter++;
                }
            }
            return counter;
        }
    }
}
