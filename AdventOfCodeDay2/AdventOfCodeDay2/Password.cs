﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdventOfCodeDay2
{
    class Password
    {
        //private int minValue;
        //private int maxValue;
        //private string letter;
        //private string thePassword;

        public Password(int minValue, int maxValue, char letter, string thePassword)
        {
            MinValue = minValue;
            MaxValue = maxValue;
            Letter = letter;
            ThePassword = thePassword;
        }
        public int MinValue
        {
            get; set;
        }
        public int MaxValue
        {
            get; set;
        }

        public char Letter
        {
            get; set;
        }

        public string ThePassword
        {
            get; set;
        }

    }
}
