﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCodeDay1
{
    class Program
    {
        static void Main(string[] args)
        {
            var numberFile = File.ReadAllLines(@"Day1.txt");
            var numberList = new List<int>();

            foreach (string line in numberFile)
            {
                numberList.Add(Int32.Parse(line));
            }

            var pairList = GetPair(numberList);

            var result = MultiplyPair(pairList);

            Console.WriteLine(result.ToString());


        }

        public static List<int> GetPair(List<int> numberList)
        {
            var pairList = new List<int>();

            for (int i = 0; i < numberList.Count; i++)
            {
                for (int j = i + 1; j < numberList.Count; j++)
                {
                    for (int k = j + 1; k < numberList.Count; k++)
                    {
                        if (numberList[i] + numberList[j] + numberList[k] == 2020)
                        {
                            pairList.Add(numberList[i]);
                            pairList.Add(numberList[j]);
                            pairList.Add(numberList[k]);
                            return pairList;
                        }
                    }

                }
            }

            return pairList;


        }
        public static int MultiplyPair(List<int> pairList)
        {
            var sum = pairList.Aggregate(1, (x, y) => x * y);
            return sum;

        }
    }
}
