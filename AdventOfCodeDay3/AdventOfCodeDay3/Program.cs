﻿using System;
using System.IO;
using System.Linq;

namespace AdventOfCodeDay3
{
    class Program
    {
        static void Main(string[] args)
        {
            var file = Input();
            var tree = CountTree(file, 1, 3);
            var totaltSum = CountTreeDifferentPatter(file);
            Console.WriteLine(tree.ToString());
            Console.WriteLine(totaltSum.ToString());
        }
        public static string[] Input()
        {


            var mapFile = File.ReadAllLines(@"Day3.txt");

            return mapFile;

        }

        public static int CountTree(string[] input, int yMove, int xMove)
        {
            var x = 0;
            var y = 0;
            var count = 0;

            while (y < input.Length - yMove)
            {
                var rowLength = input[y].Length;

                x = (x + xMove) % rowLength;

                var c = input[y + yMove][x];

                if (c == '#')
                {
                    count++;
                }

                y += yMove;
            }
            return count;

        }
        public static long CountTreeDifferentPatter(string[] input)
        {
            var a = CountTree(input, 1, 1);
            var b = CountTree(input, 1, 3);
            var c = CountTree(input, 1, 5);
            var d = CountTree(input, 1, 7);
            var e = CountTree(input, 2, 1);

            var sum = a * b * c * d * (long)e;

            return sum;


        }

    }
}
