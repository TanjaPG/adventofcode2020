﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCodeDay5
{
    class Program
    {
        static void Main(string[] args)
        {
            var file = input();
            var seatList = FindAllIds(file);
            var maxValue = FindHigestId(seatList);
            var mySeat = FindMySeat(seatList);

            Console.WriteLine(maxValue.ToString());
            Console.WriteLine(mySeat.ToString());

        }

        public static string[] input()
        {
            var passwordFile = File.ReadAllLines(@"Day5.txt");
            return passwordFile;
        }

        public static List<int> FindAllIds(string[] input)
        {
            var seats = new List<int>();

            foreach (string line in input)
            {
                var allRows = Enumerable.Range(0, 128).ToArray();
                var allColumns = Enumerable.Range(0, 8).ToArray();

                foreach (char c in line)
                {
                    switch (c)
                    {
                        case 'F':
                            allRows = allRows.Take(allRows.Length / 2).ToArray();
                            break;
                        case 'B':
                            allRows = allRows.Skip(allRows.Length / 2).ToArray();
                            break;
                        case 'L':
                            allColumns = allColumns.Take(allColumns.Length / 2).ToArray();
                            break;
                        case 'R':
                            allColumns = allColumns.Skip(allColumns.Length / 2).ToArray();
                            break;

                    }

                }
                var sum = allRows[0] * 8 + allColumns[0];
                seats.Add(sum);
            }

            return seats;
        }
        public static int FindHigestId(List<int> seatIds)
        {
            var maxValue = seatIds.Max(x => x);

            return maxValue;
        }
        public static int FindMySeat(List<int> listOfSeats)
        {
            var min = listOfSeats.Min(x => x);
            var max = listOfSeats.Max(x => x);

            var result = Enumerable.Range(min, max - min).Except(listOfSeats);

            return result.First();
        }
    }
}
