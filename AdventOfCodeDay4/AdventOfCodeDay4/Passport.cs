﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace AdventOfCodeDay4
{
    class Passport
    {
        public string BirthYear
        {
            get; set;
        }
        public string IssueYear
        {
            get; set;
        }

        public string ExpirationsYear
        {
            get; set;
        }

        public string Height
        {
            get; set;
        }
        public string HairColor
        {
            get; set;
        }

        public string EyeColor
        {
            get; set;
        }

        public string PassportID
        {
            get; set;
        }
        public int? CountryID
        {
            get; set;
        }

        public bool isPassportOk()
        {
            return BirthYear != null
                && IssueYear != null
                && ExpirationsYear != null
                && Height != null
                && HairColor != null
                && EyeColor != null
                && PassportID != null;
        }

        public bool isValueInPassportOk()
        {
            if (!(Int32.Parse(BirthYear) >= 1920 && Int32.Parse(BirthYear) <= 2002) || BirthYear.Length > 4)
                return false;
            if (!(Int32.Parse(IssueYear) >= 2010 && Int32.Parse(IssueYear) <= 2020) || IssueYear.Length > 4)
                return false;
            if (!(Int32.Parse(ExpirationsYear) >= 2020 && Int32.Parse(ExpirationsYear) <= 2030) || ExpirationsYear.Length > 4)
                return false;
            if (!checkHeight())
                return false;
            if (Regex.Matches(HairColor, @"^#(?:[0-9a-fA-F]{3}){2}$").Count == 0)
                return false;
            if (!checkEyeColor())
                return false;
            if ((Regex.Matches(PassportID, @"[0-9]+").Count == 0) || (PassportID.Length != 9))
                return false;

           return true;
        }
        private bool checkHeight()
        {
            if (Height.Contains("cm"))
            {
                var newHeight = Height.Remove(Height.Length - 2);
                if (Int32.Parse(newHeight) >= 150 && Int32.Parse(newHeight) <= 193)
                {
                    return true;
                }
                return false;
            }
            else if (Height.Contains("in"))
            {
                var newHeight = Height.Remove(Height.Length - 2);
                if (Int32.Parse(newHeight) >= 59 && Int32.Parse(newHeight) <= 76)
                {
                    return true;
                }
                return false;
            }

            else return false;
        }
        private bool checkEyeColor()
        {
            List<string> okColor = new List<string>(new[] { "amb", "blu", "brn", "gry", "grn", "hzl", "oth" });

            if (okColor.Any(x => okColor.Contains(EyeColor)))
                return true;

            return false;

        }

    }

}
