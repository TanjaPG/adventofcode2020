﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCodeDay2
{
    class Program
    {
        static void Main(string[] args)
        {
            var inputLines = input();
            var passwordList = convertToClass(inputLines);
            var count = numberOfOkPasswords(passwordList);
            var count2 = numbersOnPositions(passwordList);

            Console.WriteLine(count);
            Console.WriteLine(count2);
        }

        public static string[] input()
        {
            var passwordFile = File.ReadAllLines(@"Day2.txt");
            return passwordFile;
        }
        public static List<Password> convertToClass(string[] input)
        {
            string[] separatingStrings = { "-", " ", ":" };
            var passwordCriteria = new List<Password>();

            foreach (string line in input)
            {

                var value = line.Split(separatingStrings, System.StringSplitOptions.RemoveEmptyEntries);
                var minValue = Int32.Parse(value[0]);
                var maxValue = Int32.Parse(value[1]);
                var letter = Convert.ToChar(value[2]);
                var thePassword = value[3];

                var password = new Password(minValue, maxValue, letter, thePassword);
                passwordCriteria.Add(password);

            }

            return passwordCriteria;
        }
        public static int numberOfOkPasswords(List<Password> passwordList)
        {
            int counter = 0;
            foreach (Password password in passwordList)
            {
                int theLetter = password.ThePassword.Count(x => x == password.Letter);
                if (theLetter >= password.MinValue && theLetter <= password.MaxValue)
                {
                    counter++;
                }

            }
            return counter;
        }

        public static int numbersOnPositions(List<Password> passwordList)
        {
            int counter = 0;
            foreach (Password password in passwordList)
            {
                var oneLetter = password.ThePassword.ToCharArray();
                if(oneLetter[password.MinValue - 1] == password.Letter && oneLetter[password.MaxValue - 1] != password.Letter)
                {
                    counter++;
                }
                else if (oneLetter[password.MinValue - 1] != password.Letter && oneLetter[password.MaxValue - 1] == password.Letter)
                {
                    counter++;
                }
            }
            return counter;

        }


    }
}
